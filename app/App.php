<?php
/**
 * Created by PhpStorm.
 * User: Yury.Golyandin
 * Date: 30.09.2020
 * Time: 21:00
 */

namespace App;

use App\Models\Processor;
use App\Models\ResultInterface;
use LeadGenerator\Generator;
use LeadGenerator\Lead;
use Monolog\Formatter\LineFormatter;
use Spatie\Async\Pool;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class App
{
    private static $disabledCategory;
    private static $log;

    public function __construct()
    {
        self::$disabledCategory = array_rand(array_flip([
            'Buy auto',
            'Buy house',
            'Get loan',
            'Cleaning',
            'Learning',
            'Car wash',
            'Repair smth',
            'Barbershop',
            'Pizza',
            'Car insurance',
            'Life insurance'
        ]), 3);

        self::$log = (new Logger('lead_log'))
            ->pushHandler((new StreamHandler('logs/log.txt', Logger::INFO))->setFormatter(new LineFormatter("%message%\n")))
            ->pushHandler(new StreamHandler('logs/debug.txt', Logger::DEBUG))
            ->pushHandler(new StreamHandler('logs/error_log.txt', Logger::ERROR));


    }
    public function run(){
        App::$log->debug('Start application');
        $generator = new Generator();


        $pool = Pool::create();
        $pool->concurrency(60);
        $pool->timeout(600);
        $startTime = microtime(true);
        $generator->generateLeads(10000, function (Lead $lead) use($pool)  {

            $disabledCategory = App::$disabledCategory;
            $pool->add(function () use ($lead, $disabledCategory) {
                return Processor::getInstance()->process($lead, $disabledCategory);
            })->then(function ($output) {
                if ($output instanceof ResultInterface) {
                    App::$log->info($output->toString());
                }
            })->catch(function (\Throwable $exception) {
                //App::$log->error($exception->getMessage());
            });

        });
        $pool->wait();
        $endTime = microtime(true);
        App::$log->debug('Status '.$pool->status());
        App::$log->debug('Process complete in '.$endTime-$startTime.' seconds');
    }
}