<?php
/**
 * Created by PhpStorm.
 * User: Yury.Golyandin
 * Date: 30.09.2020
 * Time: 21:11
 */

namespace App\Models;

interface ResultInterface
{
    public function toString(): string;
}